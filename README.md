# My Demo Project

## Setup

#### MongoDB
MongoDB setup is straight forward. WE can just run the docker container.

```shell
docker run -d --name mongo -p 27017:27017 mongo:4.4
```

#### Elasticsearch
USe Docker for setting elasticsearch and kibana

```shell
docker network create elastic
docker run -d --name es-01 --net elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.17.15
```

#### Kibana
In case to query and see data please use below command to run kibana container.

```shell
docker run -d --name kb-01 --net elastic -p 5601:5601 -e "ELASTICSEARCH_HOSTS=http://es-01:9200" docker.elastic.co/kibana/kibana:7.17.15 
```

## DB setup
So there is CommandLineRunner block in the SpringBootApplication class to insert sample data. For Sample Data we are using csv data file.

## Application Documentation
Included swagger documentation in the application. Run the application and open the below swagger-ui link.
```
http://localhost:8080/swagger-ui/index.html
```
We can the use the dashboard for documentation and as api collection for testing/



