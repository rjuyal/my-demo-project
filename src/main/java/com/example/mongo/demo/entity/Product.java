package com.example.mongo.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@CompoundIndex(name = "category_price", def = "{'category': 1, 'price': -1}")
@Document("products")
public class Product {
    @Id
    private String id;
    private String name;
    @Indexed(unique = true)
    private String sku;
    private Double price;
    private Set<String> category;
    private Long quantity;
    private String description;
    private Long createDate;
    private Long updateDate;
}
