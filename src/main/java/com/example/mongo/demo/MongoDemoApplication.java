package com.example.mongo.demo;

import com.example.mongo.demo.dto.ProductDTO;
import com.example.mongo.demo.entity.Product;
import com.example.mongo.demo.repository.ProductRepository;
import com.example.mongo.demo.service.ProductEsDAOService;
import com.example.mongo.demo.service.ProductService;
import com.opencsv.CSVReader;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
public class MongoDemoApplication {
	@Autowired
	private ProductService productService;

	public static void main(String[] args) {
		SpringApplication.run(MongoDemoApplication.class, args);
	}

	private Set<String> getCategory(String combinedCategory) {
		if(Strings.isBlank(combinedCategory)) return Collections.EMPTY_SET;
		return Arrays.stream(combinedCategory.split("\\|"))
				.flatMap(s -> Arrays.stream(s.split(">")))
				.collect(Collectors.toSet());
	}

	@Bean
	CommandLineRunner run() {
		return args -> {
			if (!productService.isCollectionEmpty())
				return;

			List<String[]> productList = null;
			Path path = Paths.get(
					ClassLoader.getSystemResource("csv/Woo_Product_Dummy_Data_Set_Simple_and_Variable.csv").toURI());
			try (Reader reader = Files.newBufferedReader(path)) {
				try (CSVReader csvReader = new CSVReader(reader)) {
					productList = csvReader.readAll();
				}
			}

			productList.remove(0);
			List<Product> products = productList.stream().map(values -> Product.builder()
					.id(UUID.randomUUID().toString())
					.name(values[3])
					.sku(values[2])
					.price(Double.parseDouble(values[24]))
					.category(getCategory(values[25]))
					.description(values[8])
					.quantity(Long.parseLong(values[14]))
					.createDate(System.currentTimeMillis())
					.updateDate(System.currentTimeMillis())
					.build()).collect(Collectors.toList());

			productService.saveBulk(products);
		};
	}

}
