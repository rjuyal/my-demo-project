package com.example.mongo.demo.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.aggregations.*;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.example.mongo.demo.entity.Product;
import com.example.mongo.demo.entity.ProductOverPrice;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ProductEsDAOService {
    private final ElasticsearchClient elasticsearchClient;
    private static final String INDEX = "products";
    private static final String CATEGORY_FIELD_NAME = "category";
    private static final String PRICE_FIELD_NAME = "price";

    @SneakyThrows
    public void save(Product product) {
        elasticsearchClient.index(IndexRequest.of(i -> i.index(INDEX)
                .id(product.getId())
                .document(product)));
    }

    @SneakyThrows
    public Map<String, Long> getProductCountByCategory() {
        Aggregation aggregation = TermsAggregation.of(aggs -> aggs
                .field(INDEX)
                .size(30))._toAggregation();
        SearchResponse<Void> response = elasticsearchClient.search(s -> s
                .size(0)
                .index(INDEX)
                .aggregations("CategoryCount", aggregation), Void.class);

        return response.aggregations().get("CategoryCount")
                .sterms()
                .buckets()
                .array()
                .stream().collect(Collectors.toMap(s -> s.key()._toJsonString(), StringTermsBucket::docCount));
    }
    @SneakyThrows
    public List<ProductOverPrice> getProductCountWithPriceRangeInterval(Double interval) {
        Aggregation aggregation = HistogramAggregation.of(hist -> hist
                .field(PRICE_FIELD_NAME)
                .interval(interval))._toAggregation();
        SearchResponse<Void> response = elasticsearchClient.search(s -> s
                .size(0)
                .index(INDEX)
                .aggregations("ProductCountByPriceRange", aggregation), Void.class);

        List<HistogramBucket> bucketList = response.aggregations().get("ProductCountByPriceRange").histogram().buckets().array();
        return IntStream.range(0, bucketList.size())
                .mapToObj(i -> ProductOverPrice.builder()
                        .from(bucketList.get(i).key())
                        .to(i == bucketList.size() - 1 ? null : bucketList.get(i + 1).key())
                        .count(bucketList.get(i).docCount())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public void bulkSave(List<Product> productList) {
        BulkRequest.Builder br = new BulkRequest.Builder();
        for (Product product: productList) {
            br.operations(op -> op
                    .index(i -> i
                            .index(INDEX)
                            .id(product.getId())
                            .document(product)));
        }

        elasticsearchClient.bulk(br.build());
    }

    @SneakyThrows
    public void deleteById(String id) {
        elasticsearchClient.delete(d -> d
                .index(INDEX)
                .id(id));
    }


    @SneakyThrows
    public Map<String, Double> getAvgPriceOverCategory() {
        Aggregation avgAggregation = AverageAggregation.of(agg -> agg
                .field(PRICE_FIELD_NAME))._toAggregation();
        Aggregation aggregation = new Aggregation.Builder()
                .terms(new TermsAggregation.Builder()
                        .field(CATEGORY_FIELD_NAME)
                        .size(30)
                        .build())
                .aggregations("AvgPrice", avgAggregation)
                .build();
        SearchResponse<Void> response = elasticsearchClient.search(s -> s
                        .size(0)
                        .index(INDEX)
                        .aggregations("Category", aggregation)
                , Void.class);



        return response.aggregations().get("Category").sterms().buckets().array()
                .stream()
                .collect(Collectors.toMap(b -> b.key()._toJsonString(),
                        b -> b.aggregations().get("AvgPrice").avg().value()));
    }
}
