package com.example.mongo.demo.service;

import com.example.mongo.demo.dto.ProductDTO;
import com.example.mongo.demo.dto.categoryfilter.CategoryPriceFilterRequest;
import com.example.mongo.demo.entity.Product;
import com.example.mongo.demo.entity.ProductOverPrice;
import com.example.mongo.demo.repository.ProductRepository;
import com.example.mongo.demo.util.ObjectUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductEsDAOService productEsDAOService;

    public Product save(ProductDTO productDTO) {
        Product product = ObjectUtils.convert(productDTO, Product.class);
        product.setId(UUID.randomUUID().toString());
        product.setCreateDate(System.currentTimeMillis());
        product.setUpdateDate(System.currentTimeMillis());

        productRepository.save(product);
        productEsDAOService.save(product);

        return product;
    }

    public Product update(ProductDTO productDTO) {
        Product product = ObjectUtils.convert(productDTO, Product.class);
        product.setUpdateDate(System.currentTimeMillis());

        productRepository.save(product);
        productEsDAOService.save(product);

        return product;
    }

    public Product getProduct(String id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Entity Not Found"));
    }

    public Product getProductBySku(String sku) {
        return productRepository.findBySku(sku);
    }

    public Page<Product> getProducts(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public void deleteProduct(String id) {
        productRepository.deleteById(id);
        productEsDAOService.deleteById(id);
    }

    public boolean isCollectionEmpty() {
        return productRepository.count() == 0;
    }

    public boolean checkProductExist(String id) {
        return productRepository.existsById(id);
    }

    public Page<Product> getProductByCategoryAndPriceBetween(CategoryPriceFilterRequest request, Integer page) {
        return productRepository.getProductByCategoryAndPriceBetween(request.getCategory(),
                request.getPrice().getLowerLimit(),
                request.getPrice().getUpperLimit(), PageRequest.of(page - 1, 10));
    }

    public Map<String, Long> getProductCountByCategory() {
        return productEsDAOService.getProductCountByCategory();
    }

    public void saveBulk(List<Product> productList) {
        productRepository.saveAll(productList);
        productEsDAOService.bulkSave(productList);
    }

    public List<ProductOverPrice> getProductCountWithPriceRangeInterval(Double interval) {
        return productEsDAOService.getProductCountWithPriceRangeInterval(interval);
    }

    public Map<String, Double> getAvgPriceOverCategory() {
        return productEsDAOService.getAvgPriceOverCategory();
    }
}
