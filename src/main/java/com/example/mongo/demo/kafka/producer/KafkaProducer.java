package com.example.mongo.demo.kafka.producer;

import com.example.mongo.demo.dto.ProductDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class KafkaProducer {
    private final KafkaTemplate<String, Object> kafkaTemplate;
    @Value("${topics.product-create-topic}")
    private String topic;

    public void sendProductRequest(ProductDTO productDTO) {
        kafkaTemplate.send("product_create", productDTO);
    }
}
