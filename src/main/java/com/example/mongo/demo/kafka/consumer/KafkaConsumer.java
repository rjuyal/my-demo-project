package com.example.mongo.demo.kafka.consumer;

import com.example.mongo.demo.dto.ProductDTO;
import com.example.mongo.demo.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@RequiredArgsConstructor
public class KafkaConsumer {
    private final ProductService productService;

    @KafkaListener(topics = "${topics.product-create-topic}", groupId = "my-group-Id")
    public void consume(ProductDTO productDTO) {
        System.out.println(productDTO.toString());
        System.out.println("Do you approve corresponding product creation (Y/N)");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        if (s.equals("Y")) {
            productService.save(productDTO);
        }
    }
}
