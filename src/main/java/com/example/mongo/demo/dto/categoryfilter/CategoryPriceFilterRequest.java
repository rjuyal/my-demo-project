package com.example.mongo.demo.dto.categoryfilter;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CategoryPriceFilterRequest {
    @NotBlank(message = "Category can't be Blank")
    private String category;
    @Valid
    private PriceFilter price;
}
