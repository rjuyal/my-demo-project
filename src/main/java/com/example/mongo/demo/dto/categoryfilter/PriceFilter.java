package com.example.mongo.demo.dto.categoryfilter;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PriceFilter {
    @NotNull(message = "Price Lower Limit Can't be empty")
    private Double lowerLimit;
    @NotNull(message = "Price Upper Limit Can't be empty")
    private Double upperLimit;
}
