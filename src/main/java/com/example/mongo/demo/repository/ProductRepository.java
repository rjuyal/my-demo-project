package com.example.mongo.demo.repository;

import com.example.mongo.demo.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<Product, String>, PagingAndSortingRepository<Product, String> {

    @Query("{'category' : ?0, 'price' : {$gte: ?1, $lte: ?2 } }")
    Page<Product> getProductByCategoryAndPriceBetween(String category, Double lowerLimit, Double upperLimit, Pageable pageable);

    Product findBySku(String sku);
}
