package com.example.mongo.demo.controller;

import com.example.mongo.demo.dto.ProductDTO;
import com.example.mongo.demo.dto.categoryfilter.CategoryPriceFilterRequest;
import com.example.mongo.demo.entity.Product;
import com.example.mongo.demo.entity.ProductOverPrice;
import com.example.mongo.demo.kafka.producer.KafkaProducer;
import com.example.mongo.demo.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final KafkaProducer kafkaProducer;

    @Operation(summary = "Get Products in a pageable manner")
    @GetMapping
    public Page<Product> getProducts(@ParameterObject Pageable pageable) {
        return productService.getProducts(pageable);
    }

    @Operation(summary = "Get Product by Id")
    @GetMapping("/{id}")
    public Product getProducts(@PathVariable String id) {
        return productService.getProduct(id);
    }

    @Operation(summary = "Get Product count by category in the catalog. This is done using Elasticsearch aggregation.")
    @GetMapping("/products-count-by-category")
    public Map<String, Long> getProductCountByCategory() {
        return productService.getProductCountByCategory();
    }

    @Operation(summary = "Get product by using sku id. THis to show Indexing functionality for this field.")
    @GetMapping("/sku/{id}")
    public Product getProductBySku(@PathVariable String id) {
        return productService.getProductBySku(id);
    }

    @Operation(summary = "Create Product using given request json. This will send request to kafka which need to be approved.")
    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public String createProduct(@RequestBody ProductDTO product) {
        kafkaProducer.sendProductRequest(product);
        return "Request Send Successfully";
    }

    @Operation(summary = "Update Product using given request")
    @PutMapping()
    public Product updateProduct(@RequestBody ProductDTO product) {
        if (!productService.checkProductExist(product.getId()))
            throw new RuntimeException("Product does not exist");
        return productService.update(product);
    }

    @Operation(summary = "Get the given category products between price limit. This is to show compound index for category and price.")
    @PostMapping("/category-price")
    public Page<Product> getProductByProductAndPriceLimit(@RequestBody @Valid CategoryPriceFilterRequest categoryPriceFilterRequest, @RequestParam @Min(1) Integer page) {
        return productService.getProductByCategoryAndPriceBetween(categoryPriceFilterRequest, page);
    }

    @Operation(summary = "Get price range of the products with the given interval.")
    @GetMapping("/product-price-range-count")
    public List<ProductOverPrice> getProductCountWithPriceRangeInterval(@RequestParam(defaultValue = "10")
                                                                            @DecimalMin(value = "0.1") BigDecimal interval) {
        return productService.getProductCountWithPriceRangeInterval(interval.doubleValue());
    }

    @Operation(summary = "Get Average Price across all product categories.")
    @GetMapping("/avg-price-over-category")
    public Map<String, Double> getAvgPriceOverCategory() {
        return productService.getAvgPriceOverCategory();
    }

    @Operation(summary = "Delete Product for given Id")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> updateProduct(@PathVariable("id") String id) {
        productService.deleteProduct(id);
        return ResponseEntity.ok("Entity Deleted Successfully");
    }
}
